package it.mc.infosons.service;

import it.mc.infosons.entity.Sede;
import it.mc.infosons.dao.OfficeDao;
import java.util.List;

/**
 *
 * @author mattia
 */
public class OfficeService {
    
    private final OfficeDao officeDao;
    
    public OfficeService(){
        this.officeDao = new OfficeDao();
    }
    
    public List <Sede> getAllOffice(){
        return officeDao.getAllOffice();
    }
    
    public Sede getOfficeInfo(int idSede){
        return officeDao.getInfo(idSede);
    }
    
    public void insertOffice(Sede sede){
        officeDao.insertOffice(sede);
    }

    public void deleteOffice(Sede sede){
        officeDao.deleteOffice(sede);
    }
    
}

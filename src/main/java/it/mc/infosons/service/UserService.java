package it.mc.infosons.service;

import it.mc.infosons.entity.*;
import it.mc.infosons.dao.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 *
 * @author mattia
 */
public class UserService {
    
    private final UserDao userDao;
    private final OfficeDao officeDao;
    
    public UserService(){
        this.userDao = new UserDao();
        this.officeDao = new OfficeDao();
    }
    
 
    public boolean chechLogIn(String email, String password){
        Utente utente = userDao.getInfoFromEmail(email);
        return password.equals(utente.getPassword());
        
    }
    
    public Utente getInfo(int matricola){
        return userDao.getInfo(matricola);
    }
    
    public Utente getInfoFromEmail(String username) {
        return userDao.getInfoFromEmail(username);
    }
    
    public List<Utente> getAllUser(){
        return userDao.getAllUser();
    }
    
    public boolean getStatus(Utente utente){
        
        boolean status = false;
        try{
            Date date = new Date( );
            SimpleDateFormat day = new SimpleDateFormat ("yyyy-MM-dd");
            UtSe utse = userDao.getStatus(utente, day.format(date));
            if(utse.getOraUscita() == null){
                status = true;
            }

            
        }
        catch(Exception e){
            System.err.println(e.toString());
        }
        
        return status;
    }
    
    public boolean checkDay(Utente utente){
        
        boolean status = false;
        try{
            Date date = new Date( );
            SimpleDateFormat day = new SimpleDateFormat ("yyyy.MM.dd");
            UtSe utse = userDao.getStatus(utente, day.format(date));
            if(utse.getOraUscita() != null){
                status = true;
            }

            
        }
        catch(Exception e){
            System.err.println(e.toString());
        }
        
        return status;
    }
    
    public UtSe checkIn(int matricolaUtente, int idSede){
        
        Utente utente =  userDao.getInfo(matricolaUtente);
        Sede sede = officeDao.getInfo(idSede); 
        Date date = new Date( );
        SimpleDateFormat day = new SimpleDateFormat ("yyyy-MM-dd");
        SimpleDateFormat in = new SimpleDateFormat ("hh:mm:ss");
        UtSeId utseid = new UtSeId(matricolaUtente, idSede,day.format(date));
        UtSe utse = new UtSe(utseid, sede, utente, in.format(date), null);
        userDao.checkAccess(utse);
      
        return utse;
    }
    
    public UtSe checkOut(int matricolaUtente){
            Date date = new Date( );
            SimpleDateFormat day = new SimpleDateFormat ("yyyy-MM-dd");
            SimpleDateFormat out = new SimpleDateFormat ("hh:mm:ss");
            UtSe utse = userDao.getStatus(this.getInfo(matricolaUtente), day.format(date));
            utse.setOraUscita(out.format(date));
            userDao.checkAccess(utse);
            
            return utse;
        
    }
    
    public void insertUser(Utente utente) {
        userDao.insertUser(utente);
    }
        
    public void deleteUser(Utente utente) {
        userDao.deleteUser(utente);
    }
    
    public List<UtSe> getDays(int matricola) {
        return userDao.getDays(matricola);
    }

    
}

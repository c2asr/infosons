package it.mc.infosons.dao;

import it.mc.infosons.entity.*;
import java.util.List;
import java.util.Set;
import org.hibernate.Query;


/**
 *
 * @author mattia
 */
public class UserDao extends BaseDao{
    
    
    public Utente getInfo(int matricola){
        open();
        Query q = session.createQuery("from Utente as utente where utente.matricola=" + matricola);
        Utente utente = (Utente) q.uniqueResult();
        close();
        return utente;
    }
    
    public Utente getInfoFromEmail(String email){
        open();
        Query q = session.createQuery("from Utente as utente where utente.email='" + email+"'");
        Utente utente = (Utente) q.uniqueResult();
        close();
        return utente;
    }
    
    public List<UtSe> getDays(int matricola){
                System.out.println("getDays DAO");
        open();
        Query q = session.createQuery("From UtSe as utse join fetch utse.sede as sede join utse.id as utseid where utse.utente= "+matricola+"order by utseid.data desc");
        List<UtSe> utses = q.list();
        close();
        return utses;
    }
    
    public List <Utente> getAllUser(){
        open();
        Query q = session.createQuery("From Utente as utente");
        List <Utente> utenti = q.list();
        close();
        return utenti;
    }
    
    public void checkAccess(UtSe utse){
        open();
        session.saveOrUpdate(utse);
        close();
    }
    
    
    public UtSe getStatus(Utente utente, String data){
        open();
        Query q = session.createQuery("From UtSe as utse inner join fetch utse.id as utseid where utse.utente= "+utente.getMatricola()+"AND utseid.data = '"+data+"'");
        UtSe utse = (UtSe) q.uniqueResult();
        close();
        return utse;
    }
    
    
    public void insertUser(Utente utente) {
        open();
        session.save(utente);
        close();
    }
    
    public void deleteUser(Utente utente) {
        open();
        session.delete(utente);
        close();
    }
    
}

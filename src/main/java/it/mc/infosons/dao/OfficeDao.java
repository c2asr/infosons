package it.mc.infosons.dao;

import it.mc.infosons.entity.Sede;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author mattia
 */
public class OfficeDao extends BaseDao{
        
           
    public Sede getInfo(int id){
        open();
        Query q = session.createQuery("from Sede as sede where sede.id=" + id);
        Sede sede = (Sede) q.uniqueResult();
        close();
        return sede;
    }
    
    public List <Sede> getAllOffice(){
        open();
        Query q = session.createQuery("From Sede as sede");
        List <Sede> sedi = q.list();
        close();
        return sedi;
    }
    
    public void insertOffice(Sede sede){
        open();
        session.save(sede);
        close();
    }
    
    public void deleteOffice(Sede sede){
        open();
        session.delete(sede);
        close();
    }
    
    public boolean updateOffice(Sede sede){
        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.mc.infosons.dao;

import it.mc.infosons.config.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author mattia
 */
public class BaseDao {
    
    SessionFactory sessionFactory;
    Session session = null;

    public BaseDao() {
        
        //this.sessionFactory = HibernateUtil.getSessionFactory();
        //this.session = sessionFactory.getCurrentSession();
    }
    
    public void open(){
        this.sessionFactory = HibernateUtil.getSessionFactory();
        this.session = sessionFactory.openSession();
        session.beginTransaction();
    }
    public void close(){
        session.getTransaction().commit();
        this.session.close();
    }
}

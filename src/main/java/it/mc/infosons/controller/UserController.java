package it.mc.infosons.controller;

import com.sun.jersey.api.view.Viewable;
import it.mc.infosons.entity.Sede;
import it.mc.infosons.entity.Utente;
import it.mc.infosons.service.OfficeService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import it.mc.infosons.service.UserService;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;

 

@Path("user")
public class UserController {
        
        private final UserService userService;
        private final HttpServletRequest request;
        private final HttpSession session;
        private final OfficeService officeService;
        private final HttpServletResponse response;
        
        public UserController(@Context HttpServletRequest request, @Context HttpServletResponse response) throws IOException{
            this.officeService = new OfficeService();
            this.userService = new UserService();
            this.request = request;
            this.response = response;
            this.session = request.getSession();
        }
        
        @GET
	@Path("/")
	public Viewable loginTestAction(){
            
            if(session.getAttribute("user") == null){
               return new Viewable("/index.jsp");
            }
            else{
                return indexAction();
            }
        }
        
       
        @GET
        @Path("/index")
	public Viewable indexAction() {
                List<Sede> sedi = officeService.getAllOffice();
                Utente utente = (Utente) session.getAttribute("user");
                request.setAttribute("offices", sedi);
                request.setAttribute("status", userService.getStatus(utente));
                request.setAttribute("day", userService.checkDay(utente));
                return new Viewable("/user.jsp");
        }
        
               
        @GET
	@Path("/checkin")
	public Viewable CheckInAction(@QueryParam("matricola") int matricola,
            @QueryParam("sede") int sede) {
                try{
                    userService.checkIn(matricola, sede );
                    request.setAttribute("msg", "Checkin effettuato con successo");
                }
                catch(Exception e){
                    request.setAttribute("msg", "Errore checkin");
                }
                return indexAction();
        }
        
        @GET
	@Path("/checkout")
	public Viewable CheckOutAction(@QueryParam("matricola") int matricola){
                
                try{
                    userService.checkOut(matricola);
                    request.setAttribute("msg", "Checkout effettuato con successo");
                }
                catch(Exception e){
                    request.setAttribute("msg", "Errore checkout");
                }
                return indexAction();
        }
        
        
              
        @GET
	@Path("/logout")
	public Viewable logoutAction() {
            session.removeAttribute("user");
            return new Viewable("/index.jsp");
        }
                  
              
}
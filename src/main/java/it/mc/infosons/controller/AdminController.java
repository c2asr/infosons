package it.mc.infosons.controller;

import com.sun.jersey.api.view.Viewable;
import it.mc.infosons.entity.Sede;
import it.mc.infosons.entity.Utente;
import it.mc.infosons.service.OfficeService;
import it.mc.infosons.service.UserService;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author mattia
 */
@Path("admin")
public class AdminController {
    
    private final OfficeService officeService;
    private final UserService userService;
    private final HttpServletRequest request;
    private final HttpSession session;

    
    public AdminController(@Context HttpServletRequest request) throws IOException {
        this.officeService = new OfficeService();
        this.userService = new UserService();
        this.request = request;
        this.session = request.getSession();
    }
    
    @GET
    @Path("/")
    public Viewable loginTestAction(){

        if(session.getAttribute("admin") == null){
           return new Viewable("/index.jsp");
        }
        else{
            return indexAction();
        }
    }
    
    @GET
    @Path("/index")
    public Viewable indexAction() {
            
            List<Sede> sedi = officeService.getAllOffice();
            request.setAttribute("sedi", sedi);
            List<Utente> utenti = userService.getAllUser();
            request.setAttribute("utenti", utenti);
            return new Viewable("/admin.jsp");
    }
    
    
    @POST
    @Path("/insertoffice")
    public Viewable insertOfficeAction(@FormParam("nomesede") String nomeSede,
            @FormParam("indsede") String indSede) {

        Sede sede = new Sede(nomeSede, indSede);
        try{
            officeService.insertOffice(sede);
            request.setAttribute("msg", "Sede inserita correttamente");
        }
        catch(Exception e){
                request.setAttribute("msg", e.getMessage());
        }
        return indexAction();
    }
    
    @POST
    @Path("/insertuser")
    public Viewable insertUserAction(@FormParam("nome") String nome,
            @FormParam("cognome") String cognome,
            @FormParam("email") String email,
            @FormParam("password") String password) {

        Utente utente = new Utente(email, nome, cognome, password);
        try{
            userService.insertUser(utente);
            request.setAttribute("msg", "Utente inserito correttamente");
        }
        catch(Exception e){
                request.setAttribute("msg", e.getMessage());
        }
        return indexAction();
    }
    
    @GET
    @Path("/deleteoffice")
    public Viewable deleteOfficeAction(@QueryParam("sede") int idSede){

        Sede sede = officeService.getOfficeInfo(idSede);
        try{
            officeService.deleteOffice(sede);
            request.setAttribute("msg", "Sede eliminata correttamente");
        }
        catch(Exception e){
                request.setAttribute("msg", e.getMessage());
        }
        return indexAction();
    }
    
    @GET
    @Path("/deleteuser")
    public Viewable deleteUserAction(@QueryParam("matricola") int matricola) {

        Utente utente = userService.getInfo(matricola);
        try{
            userService.deleteUser(utente);
            request.setAttribute("msg", "Utente eliminato correttamente");
        }
        catch(Exception e){
            request.setAttribute("msg", e.getMessage());
        }
        return indexAction();
    }
    
    @GET
    @Path("/getstat")
    public Viewable getStatAction(@QueryParam("matricola") int matricola) {

        try{
            userService.getDays(matricola);
            request.setAttribute("user", userService.getInfo(matricola));
            request.setAttribute("days", userService.getDays(matricola));
        }
        catch(Exception e){
            request.setAttribute("msg", e.getMessage());
        }
        return indexAction();
    }
    

    @POST
    @Path("/login-check")
    public Viewable checkLoginAction(@FormParam("admin") String admin,
        @FormParam("password") String password) throws IOException {

        if(admin.equals("admin") && password.equals("info1234")){
            session.setAttribute("admin", "logged");
            return indexAction();
        }
        else{
            return new Viewable("/loginadmin.jsp");
        }

    }
        
        
    @GET
    @Path("/logout")
    public Viewable logoutAction() throws IOException {
        session.removeAttribute("admin");
        return new Viewable("/index.jsp");
    }
}

package it.mc.infosons.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import it.mc.infosons.service.UserService;

/**
 *
 * @author mattia
 */
@WebServlet("/login")
public class LoginController extends HttpServlet{
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        String username=request.getParameter("username");
        String password=request.getParameter("password");
        
        
        UserService userService = new UserService();
        HttpSession session = request.getSession();
        
        String path = request.getContextPath();
        
        try{
            if(username.equals("admin") && password.equals("info1234")){
                session.setAttribute("admin", "logged");
                response.sendRedirect(path+"/admin");
            }
        
            else if(userService.chechLogIn(username, password)){
                session.setAttribute("user", userService.getInfoFromEmail(username));
                response.sendRedirect(path+"/user");
            }
            else{
                response.sendRedirect(path+"/login");
            }
        }
        catch(NullPointerException e){
            request.setAttribute("msg", "Errore Login");
            RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
            rd.forward(request, response);
        }
        
        
        
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
            response.sendRedirect("/accessi/login");
    }

}

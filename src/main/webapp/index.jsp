<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Accessi</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/login.css">


  </head>

  <body>

    <div class="container">

      <form class="form-signin" role="form" action="<c:out value="${pageContext.servletContext.contextPath}" />/login" method="post">
        <h2 class="form-signin-heading text-center">Log In</h2>
        <input type="text" name ="username" class="form-control" placeholder="username" required autofocus>
        <input type="password" name="password" class="form-control" placeholder="Password" required>
        <c:if test="${msg != null}">
            <div class="alert alert-danger" role="alert">${msg}</div>
        </c:if>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Accedi</button>
      </form>

    </div> <!-- /container -->

  </body>
</html>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Accessi - Admin</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.css">
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/dashboard.css">
    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<c:out value="${pageContext.servletContext.contextPath}" />/admin/">Infosons</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">Utente<span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="javascript:showModal('insert-user')">Aggiungi</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">Sede<span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="javascript:showModal('insert-office')">Aggiungi</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">Admin<span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/admin/logout">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        <li class="active"><a href="<c:out value="${pageContext.servletContext.contextPath}" />/admin/">Home</a></li>
                    </ul>
                </div>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <h1 class="page-header">Dashboard Admin</h1>
                    <c:if test="${msg != null}">
                        <div role="alert" class="alert alert-warning fade in">
                            <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <strong>Attezione!</strong> ${msg}
                          </div>
                    </c:if>
                    <c:if test="${days != null}">
                    <div id = "stat">
                        <h2>Statistiche ${user.getNome()} ${user.getCognome()}</h2>
                        <b>Giorni lavorativi</b>
                         <table class="table table-striped">
                            <thead>
                                <td>Giorno</td>
                                <td>Ora entrata</td>
                                <td>Ora uscita</td>
                                <td>Sede</td>
                            </thead>

                            <c:forEach var="giorno" items="${days}">
                            <tr>
                                <td><c:out value="${giorno.getId().getData()}"/></td>
                                <td><c:out value="${giorno.getOraEntrata()}"/></td>
                                <td><c:out value="${giorno.getOraUscita()}"/></td>
                                <td><c:out value="${giorno.getSede().getName()}"/></td>
                             </tr>   
                            </c:forEach>

                        </table>
                        <a href="javascript:hideDiv('stat');">Chiudi statistiche</a>
                    </div>

                </c:if>
                    <br>
                        <h2 class="sub-header">Sedi</h2>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Indirizzo</th>
                                        <th>Elimina</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="office" items="${sedi}">
                                        <tr>
                                            <td><c:out value="${office.getName()}"/></td>
                                            <td><c:out value="${office.getIndirizzo()}"/></td>
                                            <td><a href="<c:out value="${pageContext.servletContext.contextPath}" />/admin/deleteoffice?sede=<c:out value="${office.getId()}"/>" style="color: tomato;"/><span class="glyphicon glyphicon-trash"></span></a><br><br></td>
                                        </tr>
                                    </c:forEach>

                                </tbody>
                            </table>
                        </div>
                        <br>
                        <h2 class="sub-header">Utenti</h2>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Cognome</th>
                                        <th>Matricola</th>
                                        <th>Email</th>
                                        <th>Elimina</th>
                                        <th>Stat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="utente" items="${utenti}">
                                        <tr>
                                        <td><c:out value="${utente.getNome()}"/></td>
                                        <td><c:out value="${utente.getCognome()}"/></td>
                                        <td><c:out value="${utente.getMatricola()}"/></td>
                                        <td><c:out value="${utente.getEmail()}"/></td>
                                        <td><a href="<c:out value="${pageContext.servletContext.contextPath}" />/admin/deleteuser?matricola=<c:out value="${utente.getMatricola()}"/>" style="color: tomato;"/><span class="glyphicon glyphicon-trash"></span></a></td>
                                        <td><a href="<c:out value="${pageContext.servletContext.contextPath}" />/admin/getstat?matricola=<c:out value="${utente.getMatricola()}"/>" style="color: blue;"/><span class="glyphicon glyphicon-stats"></span></a></td>
                                        </tr>
                                    </c:forEach>

                                </tbody>
                            </table>
                        </div>



                </div>
            </div>
        </div>
        
        <!-- Modal Add Office-->
            <div class="modal fade" id="modal-insert-office" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Inserimento Sede</h4>
                        </div>
                        <div class="modal-body">
                            <form action="${pageContext.servletContext.contextPath}/admin/insertoffice/" method="post">
                                <input type="text" name = "nomesede" class="form-control" placeholder="Nome della sede" required><br>
                                <input type="text" name = "indsede" class="form-control" placeholder="Indirizzo della sede" required><br>
                                <div class="modal-footer">
                            <button type="submit" class="btn btn-default" >Inserisci Sede</button>
                        </div>
                            </form>
                        </div>
                        
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            
            <!-- Modal Add User-->
            <div class="modal fade" id="modal-insert-user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Inserimento Utente</h4>
                        </div>
                        <div class="modal-body">
                            <form action="${pageContext.servletContext.contextPath}/admin/insertuser/" method="post">
                                <input type="text" name = "nome" class="form-control" placeholder="Nome dell'utente" required><br>
                                <input type="text" name = "cognome" class="form-control" placeholder="Cognome dell'utente" required><br>
                                <input type="text" name = "email" class="form-control" placeholder="Email dell'utente" required><br>
                                <input type="text" name = "password" class="form-control" placeholder="Password dell'utente" required><br>
                            
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-default"  value="Inserisci Utente">
                        </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
        <script src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.11.0.js"></script>
        <script src="${pageContext.servletContext.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/resources/js/docs.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/resources/js/system.js"></script>
    </body>
</html>


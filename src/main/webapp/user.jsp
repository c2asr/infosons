<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Accessi - ${user.getNome()} ${user.getCognome()}</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.css">
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/dashboard.css">
    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<c:out value="${pageContext.servletContext.contextPath}" />/user/">Infosons</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">${user.getNome()} ${user.getCognome()}<span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <c:if test="${status == true}">
                                    <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/user/checkout?matricola=${user.getMatricola()}">CheckOut</a></li>
                                    </c:if>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/user/logout">Logout</a></li>
                            </ul>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        <li class="active"><a href="<c:out value="${pageContext.servletContext.contextPath}" />/user/">Home</a></li>
                    </ul>
                </div>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <h1 class="page-header">Dashboard</h1>
                    <c:if test="${status == true}">
                        <div class="alert alert-success" role="alert">Hai effettuato il check in</div>
                    </c:if>
                    <c:if test="${day == true}">
                        <div class="alert alert-success" role="alert">Hai terminato la tua giornata lavorativa</div>
                    </c:if>
                
                    <c:if test="${msg != null}">
                        <div role="alert" class="alert alert-warning fade in">
                            <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <strong>Attezione!</strong> ${msg}
                          </div>
                    </c:if>
                   
                    <c:if test="${(status == false) && (day == false)}">
                        <h2 class="sub-header">Sedi</h2>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Indirizzo</th>
                                        <th>CheckIn</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="office" items="${offices}">
                                        <tr>
                                            <td><c:out value="${office.getName()}"/></td>
                                            <td><c:out value="${office.getIndirizzo()}"/></td>
                                            <td><a href="<c:out value="${pageContext.servletContext.contextPath}" />/user/checkin?matricola=${user.getMatricola()}&sede=${office.getId()}"><span class="glyphicon glyphicon-ok"></span></a></td>
                                        </tr>
                                    </c:forEach>

                                </tbody>
                            </table>
                        </div>

                    </c:if>



                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.11.0.js"></script>
        <script src="${pageContext.servletContext.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/resources/js/docs.min.js"></script>
    </body>
</html>


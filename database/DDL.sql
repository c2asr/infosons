DROP USER 'accessiUser'@'localhost';
CREATE USER 'accessiUser'@'localhost' IDENTIFIED BY 'accessiPassword';
GRANT ALL PRIVILEGES ON `accessi`.* TO 'accessiUser'@'localhost';

DROP DATABASE IF EXISTS `accessi`;
CREATE DATABASE `accessi`;
USE `accessi`;

CREATE TABLE IF NOT EXISTS `Utente` (
    `matricola` INTEGER UNSIGNED AUTO_INCREMENT,
    `email` VARCHAR(254) UNIQUE NOT NULL,
    `nome` VARCHAR(64) NOT NULL,
    `cognome` VARCHAR(40) NOT NULL,
    `password` VARCHAR(32) NOT NULL,
    PRIMARY KEY (`matricola`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `Sede` (
    `ID` INTEGER UNSIGNED AUTO_INCREMENT,
    `name` VARCHAR(64) NOT NULL,
    `indirizzo` VARCHAR(254) NOT NULL,
    PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `Ut_Se` (
    `matricolaUtente` INTEGER UNSIGNED NOT NULL,
    `idSede` INTEGER UNSIGNED NOT NULL,
    `data` DATE NOT NULL,
    `oraEntrata` TIME NOT NULL,
    `oraUscita` TIME,
    PRIMARY KEY (`matricolaUtente`,`idSede`,`data`),
    FOREIGN KEY (`matricolaUtente`)
	    REFERENCES `Utente` (`matricola`)
	    ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (`idSede`)
	    REFERENCES `Sede` (`ID`)
	    ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

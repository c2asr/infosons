SIMPLE USER CHECKIN CHECKOUT
============================

Semplice gestore delle presenze dei dipendenti di un'azienda

Istruzioni per la compilazione:

- mvn compile war:war
- effettuare il source dei file database.sql ed insert.sql presenti nella cartella database

Instruzioni accesso al pannello utente:

- Accedere al pannello di admin (user: admin, password: info1234) e creare un nuovo utente
- Accedere al pannello utente con le nuove credenziali (username: email inserita durante la creazione dell'utente)